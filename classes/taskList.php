<?php

require_once '../classes/dbHandler.php';

class TaskList {

	private $taskList;

	// Init class with default values.
	public function __construct() {

		$this->taskList = NULL;
	}

	public function getAllTasks() {

		$DBH = new DatabaseHandler();
		$this->taskList = $DBH->getAllTasks();
		$this->sortByPriority();
		//error_log("Taskdump: ".var_export($this->taskList, true).")\r\n", 3, "../debug.log");

		$tasksAsKeyVal = array();
		foreach ($this->taskList as $i) {
			array_push($tasksAsKeyVal, $i->getSelfAsKeyValPairs());
		}

		return $tasksAsKeyVal;
	}

	// custom sort function to facilitate usort
	private function compareTask($a, $b) {
		return $a->getInitialPriority() == $b->getInitialPriority() ? 0 : ( $a->getInitialPriority() > $b->getInitialPriority() ) ? -11 : 1;
	}

	private function sortByPriority() {
		usort($this->taskList, array($this, "compareTask"));
	}
}