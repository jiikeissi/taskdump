<?php

require_once '../classes/dbHandler.php';

// Task-class describes a single to-do item and the required functionality.
// It takes user input, can sanitize/verify it and write itself to an SQLite database.
class Task {

	// ID values for possible social features. Most of these should be cdervied from
	// TaskList and an extended class SocialTask could be created for shared/collaborative tasks with necessary values		
	// private $creatorId;
	// private $ownerId;
	// private $readerIDs[]; 
	// prviate $editorIDs[];

	private $id;
	private $title;
	private $description;
	private $initialPriority;

	// private $duration;
	// private $color;

	
	// private $magicPrio;

	// Init class with default values.
	public function __construct() {

		$this->id = NULL;
		$this->title = NULL;
		$this->description = NULL;
		$this->initialPriority = NULL;
	}

	public function setId($i) {

		// TODO: need some error handling here, since this value comes directly from the db-engine.
		// if something goes wrong in db querying/handling and no id-number is received, there's gonna be problems.
		$this->id = $i;
	}

	private function getId() {
		return $this->id;
	}

	// Setter for Task $title. Escapes HTML special chars. Returns true for ok and false for nulls.
	public function setTitle($t) {

		error_log("Setting task title (current task title: ".$t.". empty?: ".var_export(empty($t), true).")\r\n", 3, "../debug.log");

		if(!empty($t)) {
			$this->title = htmlspecialchars($t, ENT_QUOTES);
			return true;
		}

		return false;
	}

	// Getter for Task $title
	public function getTitle() {
		return $this->title;
	}

	// Setter for Task $description. Defaults to null, only set if input provided.
	public function setDescription($d) {	

		if (!empty($d))
		{
			$this->description = htmlspecialchars($d, ENT_QUOTES);
		} else {
			$this->description = "No description";
		}	
	}

	// Getter for Task $description.
	public function getDescription() {
		return $this->description;
	}

	public function setInitialPriority($p) {
		if (isset($p) && is_numeric($p))
			$this->initialPriority = htmlspecialchars($p, ENT_QUOTES);
		// if no value was provided by the user, use the default of three
		else				
			$this->initialPriority = 3;
	}

	public function getInitialPriority() {
		return $this->initialPriority;
	}

	public function getSelfAsKeyValPairs() {
		$task = array();
		$task['id'] = $this->getId();
		$task['title'] = $this->getTitle();
		$task['description'] = $this->getDescription();
		$task['initialPriority'] = $this->getInitialPriority();

		return $task;
	}

	// Check provided input for nulls and set values accordingly. Generate necessary errors
	// and store them into an associative array.
	private function verifyInputAndSetMemberVars($arr, &$result) {

		$titleOK = $this->setTitle($arr["task-title"]);
		if (!$titleOK)
			array_push($result['error'], 'Please specify a title for your task');

		$this->setDescription($arr["task-description"]);

		$this->setInitialPriority($arr["task-initialPriority"]);

		if(isset($result['error']) && count($result['error']) > 0) {
			$result['success'] = false;
		} else {
				$result['success'] = true;
		}  	

		error_log("Verification state: ".var_export($result, true).")\r\n", 3, "../debug.log");
	}

	// Writes the task in to a SQLite db after verification measures.
	// Returns errors in an associative array that can be sent to the view.
	public function writeToDB($arr) {
		$result = array();
		$result['error'] = array();

		$this->verifyInputAndSetMemberVars($arr, $result);

		// If no errors were generated during input verification,
		// proceed writing the task in to the db.
		if ($result['success'] == true) {
			$DBH = new DatabaseHandler();

			// insert task, store the id-number received from the db engine
			$this->setId($DBH->addNewTaskToDb($this, $result));
		}

		// PDO exceptions are (for now) gathered in to the array, so this is used to check that none
		// were generated during db write operations.
		if(isset($result['error']) && count($result['error']) > 0) {
			$result['success'] = false;
		} else {
				$result['success'] = true;
		}  

		// add current task values in to the return value
		$result['task'] = array();
		array_push($result['task'], $this->getSelfAsKeyValPairs());

		return $result;
	}

}