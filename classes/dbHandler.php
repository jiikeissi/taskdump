<?php

	require_once '../classes/task.php';

	// DatabaseHandler is responsible for all db-related operations
	class DatabaseHandler
	{

		private $DBH;	// database handle

		public function	__construct() {

			// just assigning this here statically for now...
			$dbPath = "../db/task_db.db";

			// grab database handle, disgracefully terminate with errors if unsuccesful
			try {
				$dataSource = "sqlite:".$dbPath;
				$this->DBH = new PDO($dataSource);
				$this->DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch (PDOException $e) {
				// crude and rudimental error reporting
				// TODO: make it nicer :)
				echo $e->getMessage();
			}
		}

		// Adds a new task in to the db. Errors reported via the associative array passed in method params.
		// Returns the ID number of the newly inserted task.
		public function addNewTaskToDb($t, &$result) {

			try {
				// statement handle
				$STH = $this->DBH->prepare(
					'INSERT INTO tasks (title, description, initPrio) VALUES(:taskTitle, :taskDescription, :taskPriority)'
				);

				// bind params in to the prepared statement
				$title= $t->getTitle();
				$STH->bindParam(':taskTitle', $title, PDO::PARAM_STR, 8);

				$desc = $t->getDescription();				
				$STH->bindParam(':taskDescription', $desc, PDO::PARAM_STR, 8);

				$prio = $t->getInitialPriority();
				$STH->bindParam(':taskPriority', $prio, PDO::PARAM_INT);

				$STH->execute();

				// Also let's grab the id of the newly inserted task
				// I wonder if this could be done in a single query somehow?
				$STH = $this->DBH->prepare(
				     'SELECT seq FROM sqlite_sequence WHERE name = :tableName'
				);

				$STH->bindValue(':tableName', "tasks", PDO::PARAM_STR); 
				$STH->execute();
				$id = $STH->fetch();
				return $id['seq'];

			} catch (PDOException $e) {	// append all the dirty, plain error msgs in to the array
				array_push($result['error'], $e->getMessage());
			}
		}

		public function getAllTasks() {

			try {
				// statement handle
				$STH = $this->DBH->prepare(
					'SELECT * FROM tasks'
				);

				$STH->execute();

				$contents = $STH->fetchAll();

				$tasks = array();

				foreach ($contents as $i) {
					$t = new Task();
					$t->setId($i['id']);
					$t->setTitle($i['title']);
					$t->setDescription($i['description']);
					$t->setInitialPriority($i['initPrio']);
					array_push($tasks, $t);
				}

				return $tasks;

			} catch (PDOException $e) {	// append all the dirty, plain error msgs in to the array
				array_push($result['error'], $e->getMessage());
			}
		}

	}