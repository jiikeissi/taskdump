# Project Taskdump

## Description of the project
This project, currently known only as Project Taskdump, is both a hobby project of mine and in its intermediary form also a project assignment for the Network Programming class in Seinäjoki University of Applied Sciences.

### Final form of the program
The goal is to create a to-do-list/schedule creation web application. The application will take user's to-do items together with parameters like location, priority, deadline etc. It will also handle information about user's daily schedule, like what are his/her working, how much sleep (s)he'd like to have per day and how much free time. Based on this information, the program will attempt to create a priority and deadline based schedule for the user to follow to be able to complete everything there's to do on his/her to-do-list. The program will be able to split larger tasks into smaller chunks of time. The user will also be able to assign subtasks to a single task and declare dependencies between the tasks, if something needs to get done before some other task (ie, order car summer tires before scheduling the installation). It will also attempt to optimize tasks based on location, so if the user is out at his/her parents, tasks that should be done there, will be scheduled close to each other.

By maintaining a RESTful API to the backend, it'll be possible to create several different kinds of frontends.

It will also be possible for users to share their to-dos with other users and collaborate on them.

## Technology
The project is aiming to adhere to the Model-view-controller architectural pattern and to maintain a RESTful API for different kinds of frontends.

### The project uses the following technology stack
`HTML/CSS`: frontend rendering  
`JavaScript / jQuery`: frontend logic  
`AJAX`: to move data between the client and the server  
`PHP`: backend and business logic 
`SQLite`: SQL database engine  
`Apache Server (WAMP)`

### Other tools
`GIT`: for version control and possible collaboration  
`Bitbucket.org`: Github-like service, but with more finegrained control of privacy.

### Studying goals

`API`: I've never had one of my programs offer an API, so this is something I want to familiarize myself with.  
`RESTfulness`: RESTful seems to be all the rage now, so I want to familiarize myself with how RESTful APIs are built. Caching and layering requirements that are an integral part of the REST are for now out of scope as unnecessary considering the requirements they would place on the underlying infrastructure, but will be considered later.
`Client/server-programming`: I hope to further familiarize myself with web programming and the chosen technology stack and the programming languages involved.  
`Programming skills in general`: This project should also serve to build up a bit of a programming routine and make future programming more fluent and independent and less Google-fu dependant.  
`Apache server`: This project also gives me the chance to familiarize myself with setting up a simple web server, configuring a dynamic DNS that points to my home computer and set up the server in a way that only resources that are meant to be accessed from the outside are accessible.  

The technology stack was chosen, because I'm slightly familiar with it, but still haven't ever done any very advanced programming with it, so the inital learning curve won't be as steep as it would be when using completely unfamiliar technologies/langauges, but it will still pose a significant challenge. I hope to come out of this project with better skills at creating programs with this technology stack. Also worth mentioning is, that I've never done any significant amount of interactive client/server programming, so the whole concept is pretty much new to me. Completing this project should have a positive impact on my ability to learn and apply other kinds of technologies as well in a similar context. I will also be familiarizing myself with GIT version control. It could also be interesting to try out for example a noSQL database.  
  
### Current status (2016/03/18)
Currently, the program is able to insert a single task in to the database and retrieve all entries stored in to the database and present this in a very rudimentary HTTP-frontend located at [http://jiikeissi.dy.fi:8080/taskdump/](http://jiikeissi.dy.fi:8080/taskdump/). There's a simple API in place, but in its current state it's very much RESTless, although it does in my opinion adhere to the REST requirement of statelessness. The program is currently capable of simply verifying the input it's given and does in fact mitigate XSS and SQL injection vulnerabilities. Some issues and proposals of steps to take next can be found in the Bitbucket issue tracker.