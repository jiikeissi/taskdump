// takes an HTML form and converts it to JSON
// uses HTML name and value attributes to from keyval pairs
function ConvertFormToJSON(form){
	var array = jQuery(form).serializeArray();
	var json = {};

	jQuery.each(array, function() {
		json[this.name] = this.value || '';
	});

	return json;
}

jQuery(document).ready (function() {

	// load pop-up window handler
	jQuery.getScript("js/popupWindowHandler.js", function(){});

	// load save-task- to-db logic
	jQuery.getScript("js/saveTaskToDb.js", function(){});	

	// load task retrieval logic
	jQuery.getScript("js/getAllTasks.js", function(){});	
});