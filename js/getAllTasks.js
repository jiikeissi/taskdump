// get all tasks from the db and show them as a table for the user
jQuery("#getAllTasks-btn").click(function() {

	jQuery.ajax({
		type: "POST",
		url: "./controllers/getAllTasks.php",
		data: 1,
		dataType: "json",
	})			
	.done(function(results) {
		console.log(results);

		// build an html table for the user
		var taskTable = "<table border='1' style='width:100%; text-align: center'>" +
						 	"<tr>" +
						 		"<th>ID</th>" +
						 		"<th>Initial Priority</th>" +
						 		"<th>Title</th>" +
						 		"<th>Description</th>" +
						 	"</tr>";

		jQuery.each(results, function (index, value) {

			var s = "<tr>" +
						"<td>" + value['id'] + "</td>" + 
						"<td>" + value['initialPriority'] + "</td>" +
					    "<td>" + value['title'] + "</td>" +
					    "<td>" + value['description'] + "</td>" +
				    "</tr>";

			taskTable += s;
		});

		taskTable += "</table>"
		jQuery('#results').html(taskTable);

	})
	.fail(function() {
		alert("Failed AJAX call :(");
	});

});