// save task data to DB on #save-button click
jQuery("#save-btn").click(function() {
	var formData = ConvertFormToJSON("#editor");
	console.log(formData);

	jQuery.ajax({
		type: "POST",
		url: "./controllers/saveTaskToDB.php",
		data: formData,
		dataType: "json",
	})			
	.done(function(results) {
		console.log(results);

		// since even a result containing errors is a succesfull AJAX-response,
		// check whether or not any errors occurred during the processing, if not (results["error"].length == 0), business as usual
		// else, alert user with a pop-up containing the error msgs.
		if (results["error"].length == 0) {
			var s = "Succesfully added <br/><br/>" +
					"ID: " + results["task"][0]["id"] + "<br/>" +
					"Title: " + results["task"][0]["title"] + "<br/>" +
			 		"Description: " + results["task"][0]["description"] + "<br/>" +
			 		"Initial Priority: " + results["task"][0]["initialPriority"] +
			 		 "<br/><br/> to the database";
			jQuery('#results').html(s);
		} else {
			var alertText = "Errors detected! \n\n ";
			
			jQuery.each(results["error"], function (index, value) {
				alertText += value + "\n";
			});

			alertText += "\n Task NOT saved!";
			alert(alertText);
		}						
	})
	.fail(function() {
		alert("Failed AJAX call :(");
	});

	closePopup();
});