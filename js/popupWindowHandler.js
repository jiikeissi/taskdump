// functions for pop-up window handling
// adapted from: http://hallofhavoc.com/2013/05/how-to-create-an-overlay-popup-box-using-html-css-and-jquery/

// function to show a popup windows, for example to use the task editor    
function showPopup(whichpopup){
    var docHeight = jQuery(document).height(); //grab the height of the page
    var scrollTop = jQuery(window).scrollTop(); //grab the px value from the top of the page to where you're scrolling
    jQuery('.overlay-bg').show().css({'height' : docHeight}); //display your popup background and set height to the page height
    jQuery('.popup'+whichpopup).show().css({'top': scrollTop+20+'px'}); //show the appropriate popup and set the content 20px from the window top
}

 // function to close popups
function closePopup(){
    jQuery('.overlay-bg, .overlay-content').hide(); //hide the overlay
}    

// show popup when user clicks on the link
jQuery('.show-popup').click(function(event){
    event.preventDefault(); // disable normal link function so that it doesn't refresh the page
    var selectedPopup = jQuery(this).data('showpopup'); //get the corresponding popup to show
     
    showPopup(selectedPopup); //we'll pass in the popup number to our showPopup() function to show which popup we want
});    

// hide popup when user clicks on close button or if user clicks anywhere outside the container
jQuery('.close-btn, .overlay-bg').click(function(){
    closePopup();
});

// hide the popup when user presses the esc key
jQuery(document).keyup(function(e) {
    if (e.keyCode == 27) { // if user presses esc key
        closePopup();
    }
});  